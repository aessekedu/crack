#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[]) {
    if(argc < 3) {
        printf("Usage: %s input_file output_file\n", argv[0]);
        exit(1);
    }

    FILE *in;
    in = fopen(argv[1], "r");

    FILE *out;
    out = fopen(argv[2], "a");
    
    if(!in || !out) {
        exit(1);
    }

    char buffer[100];
    while(fgets(buffer, 100, in) != NULL) {
        char *pos;
        if((pos = strchr(buffer, '\n')) != NULL) {
            *pos = '\0';
        }

        char *h = md5(buffer, strlen(buffer));
        fprintf(out, "%s\n", h);
        free(h);
    }
}
